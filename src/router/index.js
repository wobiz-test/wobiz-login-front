import Vue from "vue";
import VueRouter from "vue-router";
import Principal from "../views/Principal.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "/signin"
  },
  {
    path: "/signin",
    name: "Signin",
    component: Principal
  },
  {
    path: "/recovery",
    name: "Recovery",
    component: Principal
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
