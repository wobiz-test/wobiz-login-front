# Wobiz Login Front

## Technologies :
- VueJS
- Docker
- Docker Compose

## Frameworks & Libraries :
- Bootstrap Vue
- Axios
- Vue Validate

## How to run:
- `npm install`
- `npm run serve`

## Local port:
- 8080

## How to run with Docker Compose:
- docker-compose up -d --build

## Docker port:
- 80

